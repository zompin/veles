<?php
	$data = $_POST;

	if (empty($data['name']) && empty($data['phone'])) {
		answer('Вы не заполнили поля', false);
	}

	if (empty($data['name'])) {
		answer('Вы не ввели имя', false);
	}

	if (empty($data['phone'])) {
		answer('Вы не ввели номер телефона', false);
	}

	$mail_body = "
		<h3>Заказан звонок</h3>
		<table>
			<tr>
				<td>На имя</td>
				<td>" . $data['name'] . "</td>
			</tr>
			<tr>
				<td>На номер</td>
				<td>" . $data['phone'] . "</td>
			</tr>
		</table>
	";
	$headers = 
		"From: no-replay@veles-2015.kz\r\n" .
		"Content-type: text/html; charset=utf-8\r\n"
	;
	$body .= $message."\n";

	$sended = mail('veles-2015-s@ya.ru', 'Сообщение с сайта', $mail_body, $headers);	

	if ($sended) {
		answer('Спасибо, мы Вам перезвоним', true);
	} else {
		answer('Не удалось отправить сообщение', false);
	}

	function answer($message, $status) {
		echo json_encode(array(
				'message' => $message,
				'status' => $status
			));

		if ($status == false) {
			exit;
		}
	}
?>