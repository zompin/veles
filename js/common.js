$(document).ready(function() {

	// Главный слайдер
	$('.main-slider__images').slick({
		prevArrow: '.main-slider .prev-button',
		nextArrow: '.main-slider .next-button',
		autoplay: true
	});

	// Убираем выделение текста при клике на кнопки слайдера
	$('.prev-button, .next-button').mousedown(function() {
		return false;
	});

	// Карта
	ymaps.ready(function() {
		var map = new ymaps.Map("map", {
			center: [42.908930, 71.410518], 
			zoom: 17,
			controls: []
		});

		var template = ymaps.templateLayoutFactory.createClass('<div id=\"map-mark\"></div>');
		var placeMark = new ymaps.Placemark(
				map.getCenter(), {}, {
					iconLayout: template
				}
			);
		map.geoObjects.add(placeMark);
	});

	// Кнопки для прокрутки до формы
	$('.main-slider__button, .comfortable-conditions__button, .politics__button, .distribution_button').click(function() {
		var pointToMove = $('.contacts').offset().top;
		$('html, body').animate({scrollTop: pointToMove}, 700);
	});

	// Отправка данных формы
	$('.contacts__button').click(function() {
		var name = $('#name').val();
		var phone = $('#phone').val();

		$.ajax({
			type: 'POST',
			url: '/feedback.php',
			dataType: 'json',
			data: {
				name: name,
				phone: phone
			},
			success: function(data) {
				if (data.status) {
					$('#name').val('');
					$('#phone').val('');
				}
				alert(data.message);
			}
		});
	});

	// Показывает слайдер
	$('.warehouse-images__item, .offices-promisses-images__item').click(function() {
		var srcArr =[],
			imgs = '',
			imgsArray = [];
		$(this).find('img').each(function(i, e) {
			srcArr.push($(e).attr('src'));
		});

		for (var i = 0; i < srcArr.length; i++) {
			imgs += '<img src="' + srcArr[i] + '">';
		}

		$('.lightbox__images').html(imgs);
		imgsArray = $('.lightbox__images').find('img');

		setTimeout(function() {
			$('.lightbox__images').slick({
				prevArrow: '.lightbox__prev',
				nextArrow: '.lightbox__next',
				adaptiveHeight: true,
				infinite: false
			}).on('afterChange', function(slick, curr, i) {
				if (i == 0) {
					$('.lightbox__prev').hide();
				} else if (i == imgsArray.length - 1) {
					$('.lightbox__next').hide();
				} else {
					$('.lightbox__prev, .lightbox__next').show();
				}
			});
			$('.lightbox__prev').hide();
		}, 100);

		$('.lightbox').show();
	});

	// Закрывает слайдер
	$('.lightbox__close').click(function() {
		$('.lightbox').hide();
		$('.lightbox__images').slick('unslick');
		$('.lightbox__images').empty();
	});
});

